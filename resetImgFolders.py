#!/usr/bin/python3
# Danger! This deletes all post processed images.
import support

# Folders that we want to empty
dirs = ["trainingCircles", "trainingHexagons", "trainingPentagons", "trainingSquares",
    "trainingTriangles", "testingCircles", "testingHexagons", "testingPentagons", "testingSquares", "testingTriangles"]

# Danger! This deletes all post processed images.
support.deletePostImgs(dirs)