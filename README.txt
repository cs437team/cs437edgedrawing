Readme
Alvin Gonzales
Abdulaziz Almuhanna
Maximilian Friedrich

Requirements:
-Python 3.5
-Python Libraries:
	scikit learn
- Images must be in jpg format.

Note: To do any of these steps you must be in a command prompt at the root directory
of the project. Make sure that there are no spaces in any of the path directories
(otherwise the edge drawing program won't be able to locate images).

Create a Neural Network:	
1. First we need to preprocess our test set. Note this step is a little slow.
	Type: "python genTrainImgs.py"
	Type: "python genTestImgs.py"
This will preprocess all of our training images in the input folders
and put them in the training folders. This step uses the edge drawer.
2. In the open a command prompt type: "python multilayer.py"
3. There will be two prompts. In the first prompt enter the number of 
neurons in each in hidden layer. The second prompt asks for the number
of hidden layers. Each hidden layer will have the same number of neurons (the number you
specified). Warning: keep the numbers small or you could crash your system.
4. After entering the numbers wait for the program to finish. This can take
awhile so be patient.

Run the network:
1. First we have to preprocess our image or you can use one of the images
in the testing folders (in which just copy it to the project root directory and skip step 2). 
Drop the image in the root directory.
2. Type: "SmartEdgeRouting.exe project\directory\abosolute\path\name.jpg project\directory\abosolute\path"
This will preprocess image with the edge drawer and it will create a post version of the image.
3. Run the image through the network.
	Type: "python pklexecuter.py"
4. Enter filename_post.jpg of your image.
5. Ignore the warning about a deprecated method. Check the output vector.

These are the output vectors (if it's none of these then it is definitely wrong):
Circles = vector_c1 = [0, 0, 0, 0, 0, 1]
Hexagons = vector_p1 = [0, 1, 0, 1, 0, 1]
Pentagons vector_p1 = [0, 1, 0, 1, 0, 1]
Squares = vector_s1 = [0, 1, 1, 1, 1, 1]
Triangle = vector_t1 = [1, 0, 1, 0, 0, 1]

Program File descriptions:
failedNetworks - contains networks that didn't work due to problems with Neupy.
input folders - contains training images that haven't been processed.
output folders - contains testing images that haven't been processed (not used directly).
testing folders - post processed test images with the edge drawer (not used directly).
training folders - post processed training images with the edge drawer.
1.jpg - test image.
1_post.jpg - post processed test image.
pkl file - saved neural network
genTestImgs.py -processes the test images with the edge drawer. Takes images
	in the output folders and puts it in the testing folder.
genTrainImgs.py - processes the training images with the edge detector. Takes images
	in the input folders and puts it in the training folder.
multilayer.py -creates a multilayer network and saves it in a pkl.
pklexecuter.py -processes an input image.
resetImgFolders.py - deletes all the processed training images. "python resetImgFolders.py"
SmartEdgeRouting.exe - edge drawing program.
support.py -contains common functions that we used.
OpenCVPropertySheets- for C++ source
SmartEdgeRouting - c++ source visual studio. Note: build the exe with the ide only.
	To test run it must run through the command line.

The code contains comments in them.


