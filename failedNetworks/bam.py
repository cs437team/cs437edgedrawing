#!/usr/bin/python3

# This a BAM neural network
import scipy.misc
import os
import numpy as np
from neupy import algorithms
import math
import support  # this is a common file.
import numpy as np
from neupy import algorithms

# Reads image in as an array. We need to iterate over the training
# set. To get this function to work you need to install "pillow"
# for Aziz and I the command is
# "install -c anaconda pillow"

prediction = support.getImages("trainingSquares")
# print(len(prediction))
# list = np.array(mat).tolist()

circles = support.getImages("trainingCircles")
triangles = support.getImages("trainingTriangles")
squares = support.getImages("trainingSquares")
hexagons = support.getImages("trainingHexagons")

# flatList = support.flatten(circles)
# flatList = support.flatten(list)
# flatList = support.normalize(flatList)

# Input data that should be recoginized
# matrix = np.matrix(circles)
# matrix = np.matrix(triangles)
# prediction_matrix = np.matrix(prediction)

# Output data that should be recognized
v_c0 = np.matrix([[0, 0, 0, 0, 0, 0, 0, 0, 0, 1]])
v_c1 = np.matrix([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
v_c2 = np.matrix([[0, 0, 0, 0, 0, 0, 0, 0, 1, 0]])

v_h0 = np.matrix([[0, 0, 0, 0, 0, 0, 0, 1, 0, 0]])
v_h1 = np.matrix([[0, 0, 0, 0, 0, 0, 0, 1, 0, 1]])
v_h2 = np.matrix([[0, 0, 0, 0, 0, 0, 0, 1, 1, 0]])

v_t0 = np.matrix([[0, 0, 0, 0, 0, 0, 0, 1, 1, 1]])
v_t1 = np.matrix([[0, 0, 0, 0, 0, 0, 1, 0, 0, 0]])
v_t2 = np.matrix([[0, 0, 0, 0, 0, 0, 1, 0, 0, 1]])

v_s0 = np.matrix([[0, 0, 0, 0, 0, 0, 1, 0, 1, 0]])
v_s1 = np.matrix([[0, 0, 0, 0, 0, 0, 1, 1, 0, 0]])
v_s2 = np.matrix([[0, 0, 0, 0, 0, 0, 1, 1, 0, 1]])

data = np.concatenate(
    [circles[3], circles[4]],
     axis=0)

hints = np.concatenate([v_c0, v_c1], axis=0)

bamnet = algorithms.DiscreteBAM(mode='sync')
# print("dat: ", type(data[0]))
# print("hints: ", type(hints[0]))
# print("prediction: ", type(prediction[0]))
bamnet.train(data, hints)

recovered_zero, recovered_hint = bamnet.predict_input(data[0])
#print(recovered_hint)

i=0
for r in recovered_hint:
    print(i, " ",r)
    i += 1

# print(prediction[0])