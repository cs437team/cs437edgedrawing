# #!/usr/bin/python3

# # # This a hopfield neural network
# # import scipy.misc
# # import os
# import numpy as np
# from neupy import algorithms
# import support #this is our common file. 

# # # data = np.concatenate([zero, one, two], axis=0)

# # # Reads image in as an array. We need to iterate over the training
# # # set. To get this function to work you need to install "pillow"
# # # for Aziz and I the command is
# # # "install -c anaconda pillow" 
# # # Loads training set
# circles = support.getImages("trainingCircles")
# print(circles)
# # # triangles = support.getImages("trainingTriangles")
# # # squares = support.getImages("trainingSquares")
# # # trainSet = circles + triangles + squares
# two = np.matrix([
#     1, 1, 1, 0, 0,
#     0, 0, 0, 1, 0,
#     0, 0, 0, 1, 0,
#     0, 1, 1, 0, 0,
#     1, 0, 0, 0, 0,
#     1, 1, 1, 1, 1,
# ])
# # # create the hopfield network and train.
# dhnet = algorithms.DiscreteHopfieldNetwork(mode='sync')
# dhnet.train(circles)
# # # print (circles[0])
# # # for index in range(0, len(circles)):
# # #     if circles[index] != 0 and circles[index] != 1:
# # #         print(circles[index])
# # # mat = circles[0]
# # # print(mat[0])
# # # print(len(mat[0]))


# # # get test data

# # # get the predicted result
# # # result = dhnet.predict(trainSet)

# # # input image.

# # # draw_bin_image(result.reshape((6, 5)))

# # # from neupy import environment
# # # environment.reproducible()

# # # dhnet.mode = 'async'
# # # dhnet.n_times = 400

# # # result = dhnet.predict(half_two)
# # # draw_bin_image(result.reshape((6, 5)))

# # # result = dhnet.predict(half_two)
# # # draw_bin_image(result.reshape((6, 5)))

# # # from neupy import plots
# # # import matplotlib.pyplot as plt

# # # plt.figure(figsize=(14, 12))
# # # plt.title("Hinton diagram")
# # # plots.hinton(dhnet.weight)
# # # plt.show()