#Our multilayer network
from sklearn import metrics

import numpy as np
from sklearn.externals import joblib

import support
from sklearn.neural_network import MLPClassifier
#import matplotlib.pyplot as plt

numImages = 30

# Circles vectors
vector_c1 = [0, 0, 0, 0, 0, 1]
#vector_c2 = [0, 0, 0, 0, 1, 0]
#vector_c3 = [0, 0, 0, 0, 1, 1]
#vector_c4 = [0, 0, 0, 1, 0, 0]
#vector_c5 = [0, 0, 0, 1, 0, 1]
#vector_c6 = [0, 0, 0, 1, 1, 0]
#vector_c7 = [0, 0, 0, 1, 1, 1]
#vector_c8 = [0, 0, 1, 0, 0, 0]
#vector_c9 = [0, 0, 1, 0, 0, 1]
#vector_c10= [0, 0, 1, 0, 1, 0]
c_output = [vector_c1] * numImages

# Hex vectors
vector_h1 = [0, 0, 1, 0, 1, 1]
#vector_h2 = [0, 0, 1, 1, 0, 0]
#vector_h3 = [0, 0, 1, 1, 0, 1]
#vector_h4 = [0, 0, 1, 1, 1, 0]
#vector_h5 = [0, 0, 1, 1, 1, 1]
#vector_h6 = [0, 1, 0, 0, 0, 0]
#vector_h7 = [0, 1, 0, 0, 0, 1]
#vector_h8 = [0, 1, 0, 0, 1, 0]
#vector_h9 = [0, 1, 0, 0, 1, 1]
#vector_h10= [0, 1, 0, 1, 0, 0]
h_output = [vector_h1] * numImages

# Pent vectors
vector_p1 = [0, 1, 0, 1, 0, 1]
#vector_p2 = [0, 1, 0, 1, 1, 0]
#vector_p3 = [0, 1, 0, 1, 1, 1]
#vector_p4 = [0, 1, 1, 0, 0, 0]
#vector_p5 = [0, 1, 1, 0, 0, 1]
#vector_p6 = [0, 1, 1, 0, 1, 0]
#vector_p7 = [0, 1, 1, 0, 1, 1]
#vector_p8 = [0, 1, 1, 1, 0, 0]
#vector_p9 = [0, 1, 1, 1, 0, 1]
#vector_p10= [0, 1, 1, 1, 1, 0]
p_output = [vector_p1] * numImages

# Square vectors
vector_s1 = [0, 1, 1, 1, 1, 1]
#vector_s2 = [1, 0, 0, 0, 0, 0]
#vector_s3 = [1, 0, 0, 0, 0, 1]
#vector_s4 = [1, 0, 0, 0, 1, 0]
#vector_s5 = [1, 0, 0, 0, 1, 1]
#vector_s6 = [1, 0, 0, 1, 0, 0]
#vector_s7 = [1, 0, 0, 1, 0, 1]
#vector_s8 = [1, 0, 0, 1, 1, 0]
#vector_s9 = [1, 0, 0, 1, 1, 1]
#vector_s10= [1, 0, 1, 0, 0, 0]
s_output = [vector_s1] * numImages

# Triangle vectors
vector_t1 = [1, 0, 1, 0, 0, 1]
#vector_t2 = [1, 0, 1, 0, 1, 0]
#vector_t3 = [1, 0, 1, 0, 1, 1]
#vector_t4 = [1, 0, 1, 1, 0, 0]
#vector_t5 = [1, 0, 1, 1, 0, 1]
#vector_t6 = [1, 0, 1, 1, 1, 0]
#vector_t7 = [1, 0, 1, 1, 1, 1]
#vector_t8 = [1, 1, 0, 0, 0, 0]
#vector_t9 = [1, 1, 0, 0, 0, 1]
#vector_t10= [1, 1, 0, 0, 1, 0]
t_output = [vector_t1] * numImages

# Get lists of matrices
circles = support.getImagesML("trainingCircles")
triangles = support.getImagesML("trainingTriangles")
squares = support.getImagesML("trainingSquares")
hexagons = support.getImagesML("trainingHexagons")
pentagons = support.getImagesML("trainingPentagons")

circlesPlot = support.getImages("trainingCircles")
#print(type(np.array(circlesPlot[0])))
pred_C = support.getImagesML("testingCircles")
pred_P = support.getImagesML("testingPentagons")
pred_S = support.getImagesML("testingSquares")
pred_H = support.getImagesML("testingHexagons")

# Concat elements in list together
# def concat(list):
#     data = circles[0]
#     for index in range(len(list)):
#         data = [data, circles[index]]
#     return data

# Concat the input matrices
# dataC = concat(circles)
# dataT = concat(triangles)
# dataS = concat(squares)
# dataH = concat(hexagons)
# dataP = concat(pentagons)

#format the predicition input
predictionInput = np.array(pred_C).tolist()
trainingSize =21

c_training = circles[:trainingSize]
c_testing = circles[trainingSize:]

t_training = triangles[:trainingSize]
t_testing = triangles[trainingSize:]

s_training = squares[:trainingSize]
s_testing = squares[trainingSize:]

h_training = hexagons[:trainingSize]
h_testing = hexagons[trainingSize:]

p_training = pentagons[:trainingSize]
p_testing = pentagons[trainingSize:]

#vectors
c_v_training = c_output[:trainingSize]
c_v_testing = c_output[trainingSize:]

t_v_training = t_output [:trainingSize]
t_v_testing = t_output[trainingSize:]

s_v_training = s_output[:trainingSize]
s_v_testing = s_output[trainingSize:]

h_v_training = h_output[:trainingSize]
h_v_testing = h_output[trainingSize:]

p_v_training = p_output[:trainingSize]
p_v_testing = p_output[trainingSize:]


# Concat the input
input_training = c_training + t_training + s_training + h_training + p_training
input_training = np.array(input_training).tolist()


# Concat the output
output_training = c_v_training + t_v_training + s_v_training + h_v_training + p_v_training
output_training = np.array(output_training).tolist()



# Concat the input for testing
input_testing = c_testing + t_testing + s_testing + h_testing + p_testing
input_testing = np.array(input_testing).tolist()


# Concat the output for testing
output_testing = c_v_testing + t_v_testing + s_v_testing + h_v_testing + p_v_testing
output_testing = np.array(output_testing)#.tolist()


# Make the network and train
neurons = input("number of neurons: ")
layers = input ("number of hidden layers: ")
clf = MLPClassifier(solver='adam',activation='tanh',learning_rate='adaptive', hidden_layer_sizes=((int(neurons))*int(layers)), random_state=1)
clf.fit(input_training, output_training)

joblib.dump(clf, 'clf3.pkl')

predicted = clf.predict(input_testing)

#print(type(output_testing))
#print(len(output_testing))
#print(len(predicted))
#print(type(predicted))

print("Classification report for classifier %s:\n%s\n"
      % (clf, metrics.classification_report(output_testing, predicted)))
#print("Confusion matrix:\n%s" % metrics.confusion_matrix(output_testing, predicted))