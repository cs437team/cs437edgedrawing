#!/usr/bin/python3
# common file for everything
import os
import subprocess
import scipy.misc
import numpy as np

# draws the matrix
def drawBinImage(image_matrix):
    for row in image_matrix.tolist():
        print('| ' + ' '.join(' *'[val] for val in row))

# normalizes the matrix. Any nonzero value is turned to a 1.
def normalize(imageList):
    for index in range(len(imageList)):
        if imageList[index] == 255:
            imageList[index] = 1
        else:
            imageList[index] = 0
    return imageList

# We make everything into one list (i.e. remove sublists)
flatten = lambda list: [item for sublist in list for item in sublist]

# Load images onto a list. Returns a list that has all the matrices
# Directory should be the folder.
def getImages(directory):
    newList = []
    for file in os.listdir(os.path.join(os.path.dirname(__file__), directory)):
        if file.endswith(".jpg"):
            # Open the file and store it into an ndarray
            mat = scipy.misc.imread(os.path.join(os.path.dirname(__file__), directory, file))
            # convert to a list
            list = np.array(mat).tolist()
            # convert into a list without any sublists
            flatList = flatten(list)
            # convert the image to a binary image
            flatList = normalize(flatList)
            # print(flatList)
            # create the matrix
            matrix = np.matrix(flatList)
            #return matrix
            newList.append(matrix)
    return newList

# Load images onto a list. Returns a list that has all the arrays
# Directory should be the folder.
def getImagesML(directory):
    newList = []
    for file in os.listdir(os.path.join(os.path.dirname(__file__), directory)):
        if file.endswith(".jpg"):
            # Open the file and store it into an ndarray
            mat = scipy.misc.imread(os.path.join(os.path.dirname(__file__), directory, file))
            # convert to a list
            list = np.array(mat).tolist()
            # convert into a list without any sublists
            flatList = flatten(list)
            # convert the image to a binary image
            flatList = normalize(flatList)
            # print(flatList)
            # create the matrix
            # array = np.array(flatList)
            #return matrix
            newList.append(flatList)
    return newList

# Function for preprocessing images. The dirs are list of directories.
# The images are processed from the inputDirs to the outputDirs.
def processImages(inputDirs, outputDirs):
    for index in range(len(inputDirs)):
        for file in os.listdir(os.path.join(os.path.dirname(__file__), inputDirs[index])):
            if file.endswith(".jpg"):
                string = os.path.join(os.path.dirname(__file__), "SmartEdgeRouting.exe ") + os.path.join(os.path.dirname(__file__), inputDirs[index], file) + " " + os.path.join(os.path.dirname(__file__), outputDirs[index])
                # print (string)
                subprocess.call(string, shell=False)


# Function resets the image folders.
# It deletes the processed images.abs
# Be careful modifying this.
def deletePostImgs(dirs):
    for index in range(len(dirs)):
        for file in os.listdir(os.path.join(os.path.dirname(__file__), dirs[index])):
            if file.endswith(".jpg"):
                string = os.path.join(os.path.dirname(__file__), dirs[index], file)
                print("deleting " + string)
                # Comment this out if you want to modify this function.
                os.remove(string)

def getImagesPKL(file):
    newList = []

    if file.endswith(".jpg"):
        # Open the file and store it into an ndarray
        mat = scipy.misc.imread(os.path.join(os.path.dirname(__file__), file))
        # convert to a list
        #  hfh           list = np.array(mat).tolist()
        # convert into a list without any sublists
        flatList = flatten(mat)
        # convert the image to a binary image
        flatList = normalize(flatList)
        return flatList
            # print(flatList)
            # create the matrix
            # array = np.array(flatList)
            #return matrix
            #newList.append(flatList)
            #return newList
