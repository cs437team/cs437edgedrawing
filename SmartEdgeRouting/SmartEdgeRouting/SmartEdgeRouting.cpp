// CS 586 Final Project
// Programmed by Alvin Gonzales
#include "opencv\cv.h"
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

//Constants for SE operation.
#define GRAD_THRESHOLD 36
#define ANCHOR_THRESHOLD 8
#define SIGMA 1
#define ANCHOR_FREQ 1

//Constans for image size
#define IMG_WIDTH 200
#define IMG_HEIGHT 200

// Error message if file passed is incorrect.
void showError();
// Take derivatives using Sobel operator.
void takeDerivatives(Mat &source, Mat &gradex, Mat &absGradex, Mat &gradey, Mat &absGradey);
// Get the gradient directions.
void getDirections(Mat &source, Mat &gradDir, Mat &threshold, Mat &absGradex, Mat &absGradey);
// Get anchors.
void getAnchors(Mat &threshold, Mat &anchors, Mat &gradDir);
// Determine if point is an anchor.
bool isAnchor(int row, int column, Mat &anchors);
// Determine if point is part of an edge alread.
bool isEdge(int row, int column, Mat &edges);
// Determine if point has a nonzero gradient.
bool isBoundary(int row, int column, Mat &threshold);
// Check if we should continue drawing the edge.
bool canContinue(int row, int column, Mat &anchors, Mat &edges, Mat &threshold);
// Go left
void goLeft(int row, int column, Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir);
// Go right
void goRight(int row, int column, Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir);
// Go up
void goUp(int row, int column, Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir);
// Go down
void goDown(int row, int column, Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir);
void drawEdges(Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir){
	int cols = anchors.cols;
	int rows = anchors.rows;
	//Find an anchor and draw.
	for (int row = 0; row < rows; row++){
		for (int column = 0; column < cols; column++){
			//cout<< "rows: " << row << " columns: " << column << endl;
			// Find anchor
			if(anchors.at<uchar>(row, column) != 0){
				// vertical edge so check top and bottom
				if(gradDir.at<uchar>(row, column) == 180){
					goLeft(row, column, anchors, threshold, edges, gradDir);
					goRight(row, column, anchors, threshold, edges, gradDir);
				}
				// Vertical edge check left and right.
				else if(gradDir.at<uchar>(row, column) == 90){
					goUp(row, column, anchors, threshold, edges, gradDir);
					goDown(row, column, anchors, threshold, edges, gradDir);
				}
			}
		}
	}
}

int main(int argc, char** argv){
	// Check that there is an image file being passed to the program and save directory.
	if(argc != 3){
		showError();
		return -1;
	}

	// Get the file and image.
	string file = argv[1];
	//cout<< file << endl;
	//cout<< argv[2]<<endl;
	Mat source = imread(file, 0);
	if(!source.data){
		cout << "printing error" << endl;
		showError();
		return -1;
	}
	// Resize the images before doing anything to it.
	Size size(IMG_WIDTH, IMG_HEIGHT);
	resize(source, source, size);

	// Step 1: Apply a Gaussian filter. Here the kernel is 5x5 and
	// sigmax = sigmay = 1.
	Mat blurred = Mat::zeros(source.size(), source.type());
	GaussianBlur(source, blurred, Size(5, 5), SIGMA, SIGMA);

	// Step 2: Get the derivatives in the row and column directions. This can
	// be done with the Sobel operator.
	Mat gradeRows, gradeColumns, absGradeR, absGradeC;
	takeDerivatives(blurred, gradeRows, absGradeR, gradeColumns, absGradeC);

	// Step 2.5: Get the gradient directions
	// Create lookup table. If 90 then direction is up and down.
	// If 180 then direction is left and right.
	Mat gradDir = Mat::zeros(source.size(), source.type());
	Mat threshold = Mat::zeros(source.size(), source.type());
	getDirections(source, gradDir, threshold, absGradeR, absGradeC);
	//imshow("Input", gradDir);

	// Step 3: Get the anchors
	Mat anchors = Mat::zeros(source.size(), source.type());
	getAnchors(threshold, anchors, gradDir);

	// Step 4: Draw the edges.
	Mat edges = Mat::zeros(source.size(), source.type());
	drawEdges(anchors, threshold, edges, gradDir);

	// Show the input and output. WINDOW_FREERATIO WINDOW_AUTOSIZE
	//namedWindow("Input", WINDOW_FULLSCREEN);
	//namedWindow("Thresholded", WINDOW_FULLSCREEN);
	//namedWindow("Anchors", WINDOW_FULLSCREEN);
	//namedWindow("Edges", WINDOW_FULLSCREEN);
	//imshow("Input", source);
	//imshow("Thresholded", threshold);
	//imshow("Anchors", anchors);
	//imshow("Edges", edges);

	// Split the file name at "." and strip the subfolder infront of the file.
	// Check for forward slash and backslash.
	string delimiterFS = "/"; 
	string delimiterBS = "\\";
	string delimiterP = ".";
	string fileName;
	if(file.find_last_of(delimiterFS) != -1){ // forwardslash
		// We breakup the search search into two steps because it doesn't work the other way.
		int first =  file.find_last_of(delimiterFS);
		fileName = file.substr(first + 1);
		int second = fileName.find_last_of(delimiterP);
		fileName = fileName.substr(0, second); 
	} else if (file.find_last_of(delimiterBS) != -1){ // backslash
		int first =  file.find_last_of(delimiterBS);
		fileName = file.substr(first + 1);
		int second = fileName.find_last_of(delimiterP);
		fileName = fileName.substr(0, second); 
	}else{
		fileName = file.substr(0, file.find_last_of(delimiterP));
	}
	string fileExt = file.substr(file.find_last_of(delimiterP), file.length());

	// Save the final image.
	string path(argv[2]);
	imwrite(path + "\\" + fileName + "_post" + fileExt , edges);
	cout<< fileName << fileExt << endl;
	//cout<< "original:" << source.cols << " " << source.rows <<endl;
	cout<< "anchors:" << anchors.cols << " " << anchors.rows<< endl;
	waitKey(0);
	return 0;
}

// Error message if file passed is incorrect.
void showError(){
	cout << "Usage: SmartEdgeRouting.exe ImageFile SaveLocation" << endl;
}
// Take derivatives using Sobel operator.
void takeDerivatives(Mat &source, Mat &gradeRows, Mat &absGradeR, Mat &gradeColumns, Mat &absGradeC){
	Sobel(source, gradeRows, CV_16S, 0, 1, 3, 1, 0, BORDER_DEFAULT);
	Sobel(source, gradeColumns, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT);
	convertScaleAbs(gradeRows, absGradeR);
	convertScaleAbs(gradeColumns, absGradeC);
}
// Get the gradient directions.
void getDirections(Mat &source, Mat &gradDir, Mat &threshold, Mat &absGradeR, Mat &absGradeC){
	// The matrices are continuous so it is really just one giant array.
	if (absGradeR.isContinuous() && absGradeC.isContinuous()){
		int rows = source.rows;
		int colums = source.cols;
		colums *= rows;

		// We will traverse both gradient arrays and do comparisons.
		uchar* gradRows;
		uchar* gradColumns;
		uchar* dirP;
		uchar* threshP;

		// Get the pointers to the start of the arrays.
		gradRows = absGradeR.ptr<uchar>(0);
		gradColumns = absGradeC.ptr<uchar>(0);
		dirP = gradDir.ptr<uchar>(0);
		threshP = threshold.ptr<uchar>(0);
		for (int idx = 0; idx < colums; idx++){
			int magnitude = gradRows[idx] + gradColumns[idx];
			// The magnitude of the gradient has to pass.
			if(magnitude > GRAD_THRESHOLD){
				// 90 means the direction is vertical. 180 means horizontal.
				threshP[idx] = magnitude;
				(gradRows[idx] >= gradColumns[idx]) ?  dirP[idx] = 180 : dirP[idx] = 90;
			} else {
				// else set the value in the thresholded matrix to zero (non edges)
				threshP[idx] = 0;
			}
		}
	}

}
// Get anchors.
void getAnchors(Mat &threshold, Mat &anchors, Mat &gradDir){
	Mat paddedThresh;
	Mat paddedAnchors;
	Mat paddedDirs;
	// Pad with zeros to make calculation simpler.
	copyMakeBorder(threshold, paddedThresh, 1,1,1,1, BORDER_CONSTANT, Scalar::all(0));
	copyMakeBorder(anchors, paddedAnchors, 1,1,1,1, BORDER_CONSTANT, Scalar::all(0));
	copyMakeBorder(gradDir, paddedDirs, 1,1,1,1, BORDER_CONSTANT, Scalar::all(0));
	if(paddedDirs.isContinuous()){
		int cols = paddedDirs.cols;
		int rows = paddedDirs.rows;
		// if the dir is horizontal compare with horizontal neighbors only.
		// if the dir is vertical compare with vertical neighbors only.
		for (int row = 1; row < rows - 1; row += 1){
			for (int column = 1; column < cols - 1; column += ANCHOR_FREQ){
				//Horizontal edge so check top and bottom
				if(paddedDirs.at<uchar>(row,column) == (uchar)180){
					if((paddedThresh.at<uchar>(row, column) - paddedThresh.at<uchar>(row - 1, column) >= ANCHOR_THRESHOLD) &&
						(paddedThresh.at<uchar>(row, column) - paddedThresh.at<uchar>(row + 1, column) >= ANCHOR_THRESHOLD)){
							paddedAnchors.at<uchar>(row, column) = 255;
					}
				}
				// Vertical edge check left and right.
				else if((paddedDirs.at<uchar>(row, column) == (uchar) 90)){
					if((paddedThresh.at<uchar>(row, column) - paddedThresh.at<uchar>(row, column - 1) >= ANCHOR_THRESHOLD) &&
						(paddedThresh.at<uchar>(row, column) - paddedThresh.at<uchar>(row, column + 1) >= ANCHOR_THRESHOLD)){
							paddedAnchors.at<uchar>(row, column) = 255;
					}
				}
			}
		}
		// Remove the padding.
		anchors = paddedAnchors(Rect(1, 1, cols - 2, rows - 2));
	}
}
// Determine if point is an anchor.
bool isAnchor(int row, int column, Mat &anchors){
	int rows = anchors.rows;
	int cols = anchors.cols;
	if (row >= 0 && row < rows && column >=0 && column < cols){
		return (anchors.at<uchar>(row, column) != 0);
	} else {
		return false;
	}
}
// Determine if point is part of an edge alread.
bool isEdge(int row, int column, Mat &edges){
	int rows = edges.rows;
	int cols = edges.cols;
	if (row >= 0 && row < rows && column >=0 && column < cols){
		return (edges.at<uchar>(row, column) != 0);
	} else {
		return false;
	}
}
// Determine if point has a nonzero gradient.
bool isBoundary(int row, int column, Mat &threshold){
	int rows = threshold.rows;
	int cols = threshold.cols;
	if (row >= 0 && row < rows && column >=0 && column < cols){
		return (threshold.at<uchar>(row, column) == 0);
	} else {
		// We're exiting the edges area.
		return true;
	}
}
// Check if we should continue drawing the edge.
bool canContinue(int row, int column, Mat &anchors, Mat &edges, Mat &threshold){
	return (!isAnchor(row, column, anchors) && !isEdge(row, column, edges) && !isBoundary(row, column, threshold));
}
// Go left
void goLeft(int row, int column, Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir){
	uchar edgeVal = 255;
	edges.at<uchar>(row, column) = edgeVal;
	bool done = false;
	while (!done && gradDir.at<uchar>(row, column) == 180){
		uchar left = 0, topLeft = 0, bottomLeft = 0;
		// End if we hit an anchor or another edge.
		if(!isAnchor(row - 1, column - 1, anchors) && !isAnchor(row, column - 1, anchors) 
			&& !isAnchor(row + 1, column - 1, anchors)
			&& !isEdge(row - 1, column - 1, edges) && !isEdge(row, column - 1, edges)
			&& !isEdge(row + 1, column - 1, edges)){
			// check top left.
			if(!isBoundary(row - 1, column - 1, threshold)){
				topLeft = threshold.at<uchar>(row - 1, column - 1);
			}
			// check left.
			if(!isBoundary(row, column - 1, threshold)){
				left = threshold.at<uchar>(row, column - 1);
			}
			// check bottom left.
			if(!isBoundary(row + 1, column - 1, threshold)){
				bottomLeft = threshold.at<uchar>(row + 1, column - 1);
			}
		}
		// Exit the loop
		if(topLeft == 0 && left == 0 && bottomLeft == 0){
			done = true;
		}
		// Go top left
		else if(topLeft > left && topLeft > bottomLeft){
			edges.at<uchar>(row - 1, column - 1) = edgeVal;	
			row--;
			column--;
		}
		// Go botttom left
		else if(bottomLeft > topLeft && bottomLeft > left){
			edges.at<uchar>(row + 1, column - 1) = edgeVal;
			row++;
			column--;
		}
		// Go left.
		else {
			edges.at<uchar>(row, column - 1) = edgeVal;
			column--;
		}
	}
}
// Go right
void goRight(int row, int column, Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir){
	uchar edgeVal = 255;
	edges.at<uchar>(row, column) = edgeVal;
	bool done = false;
	while (!done && gradDir.at<uchar>(row, column) == 180){
		uchar right = 0, topRight = 0, bottomRight = 0;
		// End if we hit an anchor or another edge.
		if((!isAnchor(row - 1, column + 1, anchors) && !isAnchor(row, column + 1, anchors) 
			&& !isAnchor(row + 1, column + 1, anchors)
			&& !isEdge(row - 1, column + 1, edges) && !isEdge(row, column + 1, edges)
			&& !isEdge(row + 1, column + 1, edges))){
			// check top right.
			if(!isBoundary(row - 1, column + 1, threshold)){
				topRight = threshold.at<uchar>(row - 1, column + 1);
			}
			// check right.
			if(!isBoundary(row, column + 1, threshold)){
				right = threshold.at<uchar>(row, column + 1);
			}
			// check bottom right.
			if(!isBoundary(row + 1, column + 1, threshold)){
				bottomRight = threshold.at<uchar>(row + 1, column + 1);
			}
		}
		// Exit the loop
		if(topRight == 0 && right == 0 && bottomRight == 0){
			done = true;
		}
		// Go top right
		else if(topRight > right && topRight > bottomRight){
			edges.at<uchar>(row - 1, column + 1) = edgeVal;	
			row--;
			column++;
		}
		// Go bottom right
		else if(bottomRight > topRight && bottomRight > right){
			edges.at<uchar>(row + 1, column + 1) = edgeVal;
			row++;
			column++;
		}
		// Go right.
		else {
			edges.at<uchar>(row, column + 1) = edgeVal;
			column++;
		}
	}
}
// Go up
void goUp(int row, int column, Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir){
	uchar edgeVal = 255;
	edges.at<uchar>(row, column) = edgeVal;
	bool done = false;
	while (!done && gradDir.at<uchar>(row, column) == 90){
		uchar top = 0, topLeft = 0, topRight = 0;
		// End if we hit an anchor or another edge.
		if((!isAnchor(row - 1, column - 1, anchors) && !isAnchor(row - 1, column, anchors) 
			&& !isAnchor(row - 1, column + 1, anchors)
			&& !isEdge(row - 1, column - 1, edges) && !isEdge(row - 1, column, edges)
			&& !isEdge(row - 1, column + 1, edges))){
			// check top left.
			if(!isBoundary(row - 1, column - 1, threshold)){
				topLeft = threshold.at<uchar>(row - 1, column - 1);
			}
			// check top.
			if(!isBoundary(row - 1, column, threshold)){
				top = threshold.at<uchar>(row - 1, column);
			}
			// check top right.
			if(!isBoundary(row - 1, column + 1, threshold)){
				topRight = threshold.at<uchar>(row - 1, column + 1);
			}
		}
		// Exit the loop
		if(top == 0 && topLeft == 0 && topRight == 0){
			done = true;
		}
		// Go top left
		else if(topLeft > top && topLeft > topRight){
			edges.at<uchar>(row - 1, column - 1) = edgeVal;	
			row--;
			column--;
		}
		// Go top right
		else if(topRight > top && topRight > topLeft){
			edges.at<uchar>(row - 1, column + 1) = edgeVal;
			row--;
			column++;
		}
		// Go top.
		else {
			edges.at<uchar>(row - 1, column) = edgeVal;
			row--;
		}
	}
}

// Go down
void goDown(int row, int column, Mat &anchors, Mat &threshold, Mat &edges, Mat &gradDir){
	uchar edgeVal = 255;
	edges.at<uchar>(row, column) = edgeVal;
	bool done = false;
	while (!done && gradDir.at<uchar>(row, column) == 90){
		uchar bottom = 0, bottomLeft = 0, bottomRight = 0;
		// End if we hit an anchor or another edge.
		if((!isAnchor(row + 1, column - 1, anchors) && !isAnchor(row + 1, column, anchors) 
			&& !isAnchor(row + 1, column + 1, anchors)
			&& !isEdge(row + 1, column - 1, edges) && !isEdge(row + 1, column, edges)
			&& !isEdge(row + 1, column + 1, edges))){
			// check bottom left.
			if(!isBoundary(row + 1, column - 1, threshold)){
				bottomLeft = threshold.at<uchar>(row + 1, column - 1);
			}
			// check bottom.
			if(!isBoundary(row + 1, column, threshold)){
				bottom = threshold.at<uchar>(row + 1, column);
			}
			// check bottom right.
			if(!isBoundary(row + 1, column + 1, threshold)){
				bottomRight = threshold.at<uchar>(row + 1, column + 1);
			}
		}
		// Exit the loop
		if(bottom == 0 && bottomLeft == 0 && bottomRight == 0){
			done = true;
		}
		// Go bottom left
		else if(bottomLeft > bottom && bottomLeft > bottomRight){
			edges.at<uchar>(row + 1, column - 1) = edgeVal;	
			row++;
			column--;
		}
		// Go bottom right
		else if(bottomRight > bottom && bottomRight > bottomLeft){
			edges.at<uchar>(row + 1, column + 1) = edgeVal;
			row++;
			column++;
		}
		// Go bottom.
		else {
			edges.at<uchar>(row + 1, column) = edgeVal;
			row++;
		}
	}
}