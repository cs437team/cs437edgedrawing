#!/usr/bin/python3
# This processes all the images that we have in the directories.
import os
import subprocess
import support

inputDirs = ["inputCircles", "inputHexagons", "inputPentagons", "inputSquares", "inputTriangles"]
outputDirs = ["trainingCircles", "trainingHexagons", "trainingPentagons", "trainingSquares", "trainingTriangles"]


support.processImages(inputDirs, outputDirs)