#!/usr/bin/python3
# This processes all the test images.
import os
import subprocess
import support

inputDirs = ["outputCircles", "outputHexagons", "outputPentagons", "outputSquares", "outputTriangles"]
outputDirs = ["testingCircles", "testingHexagons", "testingPentagons", "testingSquares", "testingTriangles"]

support.processImages(inputDirs, outputDirs)