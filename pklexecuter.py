# Our realworld executer
import scipy
from sklearn.externals import joblib
import numpy as np

import support

clf = joblib.load('clf3.pkl')
input = input('filename: ')
#print(input)
mat = support.getImagesPKL(str(input))

predictionInput = np.array(mat).tolist()

print(clf.predict(mat))
